import { Row, Col, Card, Button } from 'react-bootstrap';

export default function CourseCard() {
	return(
       	<Card className="p-3">
       	    <Card.Body>
       	        <Card.Title>
       	            <h4>React Bootstrap</h4>
       	        </Card.Title>
       	        <Card.Subtitle>
       	        	<h5>Description:</h5>
       	        </Card.Subtitle>
       	        <Card.Text>
       	            <h6>This is a sample course offering.</h6>
       	        </Card.Text>

       	        <Card.Subtitle>
       	        	<h5>Price:</h5>
       	        </Card.Subtitle>
       	        <Card.Text>
       	            <h6>PhP 40,000</h6>
       	        </Card.Text>
       	        <Button size="lg" variant="primary">Enroll</Button>
       	    </Card.Body>
       	</Card>
	)
}